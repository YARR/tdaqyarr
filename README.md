## To build:

Set up TDAQ:
```
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-11-02-01
```

Create a TDAQ work area:
```
mkdir tdaq_workarea && cd tdaq_workarea
```

Add a `CMakeLists.txt` file including the following lines:
```
cmake_minimum_required(VERSION 3.14.0)
project(tdaq_workarea)
find_package(TDAQ)
tdaq_work_area()
```

Clone from git:
```
git clone https://gitlab.cern.ch/YARR/tdaqyarr.git TDAQYARR
```

Initialize the build area
```
mkdir build && cd build
```

Build and install
```
cmake ..
make -j8
make install
cd ..
```

Set up runtime environment
```
source installed/setup.sh
```

Generate software repository
```
cd build
make create_sw_repo
make install
cd ..
```

In case sometimes `repository.data.xml` is not installed in `installed/share/data/TDAQYARR/sw/`,
```
cd build
make rebuild_cache
make -j8
make install
cd ..
```

## To start the partition:

Set up runtime environment
```
source installed/setup.sh
```

IPC reference file
```
source TDAQYARR/scripts/init_ipc.sh 
```

Start partitions
```
setup_daq -p ITK -d TDAQYARR/partitions/itk_part.data.xml
```

## Useful
[1] https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltCMake

[2] https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltDevTrainingl