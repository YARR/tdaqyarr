#ifndef _YARR_CONTROLLABLE_H_
#define _YARR_CONTROLLABLE_H_

#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/RunControlCommands.h"

// YARR
#include "ScanConsoleImpl.h"

namespace yarr {
  class ScanConsoleControllable 
    : public daq::rc::Controllable, boost::noncopyable {

  public:
    ScanConsoleControllable() 
      : daq::rc::Controllable() 
    {}

    void configure(const daq::rc::TransitionCmd&) override;

    void connect(const daq::rc::TransitionCmd&) override;

    void disconnect(const daq::rc::TransitionCmd&) override;

    void prepareForRun(const daq::rc::TransitionCmd&) override;

    void stopArchiving(const daq::rc::TransitionCmd&) override;

    void user(const daq::rc::UserCmd& usrCmd) override;

  private:

    void readDB();
    void changeControlConfig(const std::vector<std::string>& parameters);
    void changeConnectConfig(const std::vector<std::string>& parameters);
    void changeScanConfig(const std::vector<std::string>& parameters);
    void runScan();

    ScanConsoleImpl m_console{};

    ScanOpts m_scanOpts;
  };
} // namespace yarr

#endif