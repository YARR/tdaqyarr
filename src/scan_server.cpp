#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"

#include "ipc/core.h"
#include "pmg/pmg_initSync.h"

#include "ScanConsoleControllable.h"

int main(int argc, char *argv[])
try {
  IPCCore::init(argc, argv);

  daq::rc::CmdLineParser cmdParser(argc, argv, true);

  daq::rc::ItemCtrl itemCtrl(cmdParser,
                             std::make_shared<yarr::ScanConsoleControllable>());

  itemCtrl.init();
  itemCtrl.run();

  return 0;
} catch(daq::rc::CmdLineHelp& help_message) {
  // Don't complain, just print info
  std::cout << help_message.what();
  return 0;
} catch(std::exception &e) {
  std::cout << "Exception thrown:\n";
  std::cout << e.what();
  return 1;
}
