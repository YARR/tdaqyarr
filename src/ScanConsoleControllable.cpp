#include "ScanConsoleControllable.h"

#include <RunControl/Common/OnlineServices.h>
#include <RunControl/Common/Exceptions.h>
#include <config/Configuration.h>
#include <dal/RunControlApplicationBase.h>

#include "yarr/dal/YarrScanConsole.h"

using namespace yarr;

void ScanConsoleControllable::readDB() {
  daq::rc::OnlineServices& rcSvc = daq::rc::OnlineServices::instance();

  try {
    // Get the application from database
    const daq::core::RunControlApplicationBase& rcBase = rcSvc.getApplication();
    const yarr::dal::YarrScanConsole* rcYarr = rcBase.cast<yarr::dal::YarrScanConsole>();

    if (rcYarr) {
      // HwController config
      m_scanOpts.ctrlCfgPath = rcYarr->get_ctrlCfgPath();

      // Connectivity config
      m_scanOpts.cConfigPaths = rcYarr->get_cConfigPaths();

      // Scan config
      const std::string& scanType = rcYarr->get_scanType();
      if (not scanType.empty()) {
        m_scanOpts.scan_config_provided = true;
        m_scanOpts.scanType = scanType;
      }

      // Output directory
      m_scanOpts.outputDir = rcYarr->get_outputDir();

      // Plot
      m_scanOpts.doPlots = rcYarr->get_doPlot();

    } else {
      std::cout << "ERROR! The application is not described in the database with the proper type: " << yarr::dal::YarrScanConsole::s_class_name << "\n";
    }
  } catch(daq::config::Exception& ex) {
    // This exception can be thrown by the Configuration layer
    std::cout << "ERROR! " << ex.what() << "\n";
  } catch(daq::rc::ConfigurationIssue& ex) {
    // This exception can be thrown by the daq::rc::OnlineServices class in case of issues reading the configuration
    std::cout << "ERROR! " << ex.what() << "\n";
  }
}

void ScanConsoleControllable::changeControlConfig(const std::vector<std::string>& parameters) {
  if (parameters.empty()) {
    // TODO ers::warning
    std::cout << "ERROR: No parameters provided for CHANGE_CONTROL_CONFIG command!" << std::endl;
    return;
  }

  m_scanOpts.ctrlCfgPath = parameters[0];
}

void ScanConsoleControllable::changeConnectConfig(const std::vector<std::string>& parameters) {
  if (parameters.empty()) {
    // TODO ers::warning
    std::cout << "ERROR: No parameters provided for CHANGE_CONNECT_CONFIG command!" << std::endl;
    return;
  }

  m_scanOpts.cConfigPaths = parameters;
}

void ScanConsoleControllable::changeScanConfig(const std::vector<std::string>& parameters) {
  if (parameters.empty()) {
    // TODO ers::warning
    std::cout << "ERROR: No parameters provided for CHANGE_SCAN_CONFIG command!" << std::endl;
    return;
  }

  if (parameters[0].empty()) {
    // TODO ers::warning
    std::cout << "ERROR: Empty scanType provided for CHANGE_SCAN_CONFIG command!" << std::endl;
    return;
  }

  m_scanOpts.scan_config_provided = true;
  m_scanOpts.scanType = parameters[0];
}

void ScanConsoleControllable::runScan() {
  // Just run all steps here for now.
  // Need to refactor YARR scan client actions to better match the RC states
  // For example m_scanOpts.scanType is only actually needed in m_console.setupScan(), but it is used in m_console.loadConfig() earlier to create the output directory

  int res = m_console.init(m_scanOpts);

  res = m_console.loadConfig();
  if (res!=0) {
    std::cout << "ERROR! loadConfig\n"; // TODO throw with ers
    return;
  }

  res = m_console.initHardware();
  if (res!=0) {
    std::cout << "ERROR! initHardware\n"; // TODO throw with ers
    return;
  }

  res = m_console.configure();
  if (res!=0) {
    std::cout << "ERROR! configure\n"; // TODO throw with ers
    return;
  }

  res = m_console.setupScan();
  if (res!=0) {
    std::cout << "ERROR! setupScan\n"; // TODO throw with ers
    return;
  }

  m_console.run();
  m_console.cleanup();
  m_console.plot();
}

void ScanConsoleControllable::configure(const daq::rc::TransitionCmd& cmd) {
  std::cout << "command '" << cmd.toString() << "' received\n"; // ERS_LOG

  try {
    this->readDB();
  } catch (std::exception& ex) {
    std::cout << "ERROR: Exception caught while reading DB: " << ex.what() << "\n";
  }
}

void ScanConsoleControllable::connect(const daq::rc::TransitionCmd& cmd) {
  std::cout << "command '" << cmd.toString() << "' received\n"; // ERS_LOG
}

void ScanConsoleControllable::prepareForRun(const daq::rc::TransitionCmd& cmd) {
  std::cout << "command '" << cmd.toString() << "' received\n"; // ERS_LOG
}

void ScanConsoleControllable::stopArchiving(const daq::rc::TransitionCmd& cmd) {
  std::cout << "command '" << cmd.toString() << "' received\n"; // ERS_LOG
}

void ScanConsoleControllable::disconnect(const daq::rc::TransitionCmd& cmd) {
  std::cout << "command '" << cmd.toString() << "' received\n"; // ERS_LOG
}

void ScanConsoleControllable::user(const daq::rc::UserCmd& usrCmd) {
  auto command = usrCmd.commandName();
  const auto& params = usrCmd.commandParameters();
  std::cout << "Received RC Command: " << command << " while in state " << usrCmd.currentFSMState() << "\n";

  if (command == "CHANGE_CONTROL_CONFIG") {
    try {
      this->changeControlConfig(params);
    } catch (std::exception& ex) {
      std::cout << "ERROR: Exception caught while changing control config: " << ex.what() << "\n";
    }
  }

  else if (command == "CHANGE_CONNECT_CONFIG") {
    try {
      this->changeConnectConfig(params);
    } catch (std::exception& ex) {
      std::cout << "ERROR: Exception caught while changing connect config: " << ex.what() << "\n";
    }
  }

  else if (command == "CHANGE_SCAN_CONFIG") {
    try {
      this->changeScanConfig(params);
    } catch (std::exception& ex) {
      std::cout << "ERROR: Exception caught while changing scan config: " << ex.what() << "\n";
    }
  }

  else if (command == "RUN_SCAN") {
    try {
      this->runScan();
    } catch (std::exception& ex) {
      std::cout << "ERROR: Exception caught while running scan: " << ex.what() << "\n";
    }
  }

  // FIXME: This is a temporary solution to allow the user to run the scan console from the command line
  else if (command == "readDB") {
    this->readDB();
  } else if(command == "loadConfig") {
    auto res = m_console.loadConfig();
    if(res != 0) {
      std::cout << "ERROR!\n";
    }
  } else if(command == "initHardware") {
    auto res = m_console.initHardware();
    if(res != 0) {
      std::cout << "ERROR!\n";
    }
  } else if(command == "configure") {
    auto res = m_console.configure();
    if(res != 0) {
      std::cout << "ERROR!\n"; // ERS_ERROR(res);
    }
  } else if(command == "setupScan") {
    auto res = m_console.setupScan();
    if(res != 0) {
      std::cout << "ERROR!\n"; // ERS_ERROR(res);
    }
  } else if(command == "run") {
    m_console.run();
  } else if(command == "cleanup") {
    m_console.cleanup();
  } else if(command == "plot") {
    m_console.plot();
  } else {
    std::cout << "ERROR, scan_server doesn't recognise user command " << command << "!\n";
  }
}