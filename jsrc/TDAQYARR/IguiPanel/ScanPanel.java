package TDAQYARR.IguiPanel;

import is.Criteria;
import is.InfoEvent;
import is.RepositoryNotFoundException;
import is.SubscriptionNotFoundException;
import is.InvalidCriteriaException;
import is.AlreadySubscribedException;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import Igui.ErrorFrame;
import Igui.Igui;
import Igui.IguiConstants;
import Igui.IguiException;
import Igui.IguiLogger;
import Igui.IguiPanel;
import Igui.RunControlFSM;
import config.Configuration;
import daq.rc.CommandSender;

import yarr.YarrScanConsole;
import yarr.YarrScanConsole_Helper;

public class ScanPanel extends IguiPanel {
  private final static String PANEL_NAME = "ScanPanel";
  private final static String RESOURCES_IS_NAME = "Resources";
  private final static String MATCH = ScanPanel.RESOURCES_IS_NAME + "." + "StoplessRecovery";

  private final CommandSender rcSender;
  private final Igui mainIgui;
  private final AtomicBoolean dbChanged = new AtomicBoolean(false);
  private final ExecutorService executor = Executors.newSingleThreadExecutor();
  private final ISReceiver isReceiver;

  private final DefaultListModel<ResourceItem> listModel = new DefaultListModel<>();
  private final DefaultComboBoxModel<String> cbModel = new DefaultComboBoxModel<>();
  private final JButton b1 = new JButton("Run Scan");

  private final DefaultTableModel tableModel = new DefaultTableModel() {
    @Override
    public boolean isCellEditable(final int row, final int column) {
      if (column == 0) {
        return false;
      }

      // Determine if a cell is editable based on the RunControlFSM.State
      final String rowName = this.getValueAt(row, 0).toString();

      if (rowName == "Controller Config") {
        return controllerReconfigurable();
      }
      else if (rowName == "Connectivity Config") {
        return connectivityReconfigurable();
      }
      else if (rowName == "Scan Config") {
        return scanReconfigurable();
      }

      return false;
    }
  };

  // This class is used to be able to subscribe to IS information with specific criteria; when the information changes
  // the various infoCreated, infoDeleted, etc messages are called and we implement them to update our data structure (a list)
  private class ISReceiver extends is.InfoWatcher {
    public ISReceiver(final ipc.Partition p) {
      super(p, ScanPanel.RESOURCES_IS_NAME, new Criteria(Pattern.compile(".*StoplessRecovery.*")));
    }

    @Override
    public void infoCreated(final InfoEvent info) {
      this.infoUpdated(info);
    }

    @Override
    public void infoDeleted(final InfoEvent info) {
      final String name = info.getName();
      final int index = name.indexOf(ScanPanel.MATCH);
      ScanPanel.this.removeResource(index != -1 ? name.substring(index + ScanPanel.MATCH.length()) : name, info.getTime().getTime());
    }

    @Override
    public void infoUpdated(final InfoEvent info) {
      final String name = info.getName();
      final int index = name.indexOf(ScanPanel.MATCH);
      ScanPanel.this.addResource(index != -1 ? name.substring(index + ScanPanel.MATCH.length()) : name, info.getTime().getTime());
    }

    @Override
    public void infoSubscribed(final InfoEvent info) {
      final String name = info.getName();
      final int index = name.indexOf(ScanPanel.MATCH);
      ScanPanel.this.addResource(index != -1 ? name.substring(index + ScanPanel.MATCH.length()) : name, info.getTime().getTime());
    }
  }

  // Helper class that represent a Resource: it has a name and a time stamp. 
  // We override the equals method: for us two resources are equal if they have the same name
  private class ResourceItem {
    final private String name;
    final private long time;

    ResourceItem(final String name, final long time) {
      this.name = name;
      this.time = time;
    }

    String name() {
      return this.name;
    }

    long time() {
      return this.time;
    }

    @Override
    public String toString() {
      return this.name + " (" + this.time + ")";
    }

    @Override
    public boolean equals(final Object other) {
      if(other instanceof ResourceItem) {
        final ResourceItem it = (ResourceItem) other;
        return this.name.equals(it.name);
      }

      return false;
    }

    @Override
    public int hashCode() {
      return this.name.hashCode();
    }
  }

  // We create our custom Exception that extends from the generic IguiException.
  // This exception may be thrown by the "panelInit" method.
  private class ScanPanelException extends IguiException {
    public ScanPanelException(final String reason, final Throwable cause) {
        super(reason, cause);
    }
  }

  // Static initializer
  {
    this.initGUI();
  }

  // This is the constructor called by the IGUI.
  public ScanPanel(final Igui mainIgui) {
    super(mainIgui);

    this.mainIgui = mainIgui;
    this.rcSender = new CommandSender(mainIgui.getPartition(), ScanPanel.PANEL_NAME);
    this.isReceiver = new ISReceiver(mainIgui.getPartition());
  }

  @Override
  public void panelInit(final RunControlFSM.State rcState, final IguiConstants.AccessControlStatus accessStatus) throws IguiException {
	  //If we are in status display mode we disable the whole panel
    this.disable(accessStatus == IguiConstants.AccessControlStatus.DISPLAY);
	  //Only if we are in the running state our buttons to send commands to applications will be enabled
    this.enableButtons(rcState == RunControlFSM.State.RUNNING);

    try {
      this.readDB();
      this.isReceiver.subscribe();
    }
    catch(final Exception ex) {
      throw new ScanPanelException("Initialization failure", ex);
    }
  }

  // If the database has been reloaded we re-initialize our panel
  @Override
  public void dbReloaded() {
    this.dbChanged.set(false);

    try {
      this.panelInit(this.mainIgui.getRCState(), this.mainIgui.getAccessLevel());
    }
    catch(final IguiException ex) {
	  // This makes a window appear
      ErrorFrame.showError(ScanPanel.PANEL_NAME, "The panel could not be re-initialized after the reload of the database", ex);
    }
  }

  // If the Access control mode changes we react to it be enabling/disabling the panel
  @Override
  public void accessControlChanged(final IguiConstants.AccessControlStatus newAccessStatus) {
    this.disable(newAccessStatus == IguiConstants.AccessControlStatus.DISPLAY);
  }

  @Override
  public String getPanelName() {
    return ScanPanel.PANEL_NAME;
  }

  @Override
  public String getTabName() {
    return ScanPanel.PANEL_NAME;
  }

  // This is executed in the panel's thread when the panel is terminated
  @Override
  public void panelTerminated() {
    try {
      this.isReceiver.unsubscribe();
    }
    catch(RepositoryNotFoundException | SubscriptionNotFoundException ex) {
    // This simply goes into the IGUI log file
      IguiLogger.warning("Failed to unsubscribe from IS server", ex);
    }

    IguiLogger.debug(this.getPanelName() + " terminated");
  }

  // If this returns "true" then the panel is notified
  @Override
  public boolean rcStateAware() {
    return true;
  }

  // Called any time there is a change in the RC FSM state.
  @Override
  public void rcStateChanged(final RunControlFSM.State oldState, final RunControlFSM.State newState) {
	// Depending on the state we may enable the buttons to send commands to hte applications
    this.enableButtons(newState == RunControlFSM.State.RUNNING);
  }

  // No IS subscription is performed, then nothing to do
  @Override
  public void refreshISSubscription() {}

  // Called every time the panel is selected.
  @Override
    public void panelSelected() {
    IguiLogger.debug(this.getPanelName() + " selected.");

    try {
      this.isReceiver.subscribe();
    }
    catch(final AlreadySubscribedException | InvalidCriteriaException | RepositoryNotFoundException ex) {
	  // This creates a message in the IGUI messages panel
      this.mainIgui.internalMessage(IguiConstants.MessageSeverity.ERROR,
                                    "IS subscription failed: some parts of the panel may not work correctly");
      IguiLogger.error("IS unsubscription failed", ex);
    }
  }

  // Called every time the panel is de-selected
  // We unsubscribe from IS
  @Override
  public void panelDeselected() {
    IguiLogger.debug(this.getPanelName() + " deselected.");

    if(this.dbChanged.get() == true) {
      this.showDiscardDbDialog("The \"mean value\" attribute of some application has been modified in the database");
    }

    try {
      this.isReceiver.unsubscribe();
    }
    catch(final RepositoryNotFoundException | SubscriptionNotFoundException ex) {
      IguiLogger.warning("IS unsubscription failed", ex);
    }
  }

  // Helper method to enable/disable buttons
  private void enableButtons(final boolean enable) {
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        ScanPanel.this.b1.setEnabled(enable);
      }
    });
  }

  // Helper method to add an item to the list of resources
  private void addResource(final String resourceName, final long time) {
    final ResourceItem res = new ResourceItem(resourceName, time);

    javax.swing.SwingUtilities.invokeLater(new Runnable() {
    @Override
      public void run() {
        if(ScanPanel.this.listModel.contains(res) == true) {
          ScanPanel.this.listModel.removeElement(res);
        }

        ScanPanel.this.listModel.addElement(res);
      }
    });
  }

  // Helper method to remove an item to the list of resources
  private void removeResource(final String resourceName, final long time) {
    final ResourceItem res = new ResourceItem(resourceName, time);

    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        ScanPanel.this.listModel.removeElement(res);
      }
    });
  }

  // Helper method to react to table changes
  private void reactToTableChanges(final int colValue, final int rowValue) {
    final Object obj = this.tableModel.getValueAt(rowValue, colValue);

    final String rowName = this.tableModel.getValueAt(rowValue, 0).toString();
    final String appName = this.tableModel.getColumnName(colValue);

    final RunControlFSM.State rcState = this.mainIgui.getRCState();

    if (rowName == "Controller Config") {
      if (controllerReconfigurable()) {
        this.sendUserCommand(appName, "CHANGE_CONTROL_CONFIG", new String[] {obj.toString()});
      }
    }

    else if (rowName == "Connectivity Config") {
      if (connectivityReconfigurable()) {
        this.sendUserCommand(appName, "CHANGE_CONNECT_CONFIG", new String[] {obj.toString()});
      }
    }

    else if (rowName == "Scan Config") {
      if (scanReconfigurable()) {
        this.sendUserCommand(appName, "CHANGE_SCAN_CONFIG", new String[] {obj.toString()});
      }
    }

  }

  // Helper methods to determine if a row in the table is editable depending on RunControlFSM.State
  private boolean controllerReconfigurable() {
    // Only configurable in the CONFIGURED state
    return this.mainIgui.getRCState() == RunControlFSM.State.CONFIGURED;
  }

  private boolean connectivityReconfigurable() {
    // Only configurable in the CONFIGURED state
    return this.mainIgui.getRCState() == RunControlFSM.State.CONFIGURED;
  }

  private boolean scanReconfigurable() {
    // Configurable in the CONFIGURED, CONNECTED and RUNNING states
    final RunControlFSM.State rcState = this.mainIgui.getRCState();
    return EnumSet.of(RunControlFSM.State.CONFIGURED, RunControlFSM.State.CONNECTED, RunControlFSM.State.RUNNING).contains(rcState);
  }

  // Helper method to retieve relevant information from the OKS database
  private void readDB() throws config.ConfigException 
  {
    final Configuration db = this.getDb();
    final dal.Partition dalPart = dal.Partition_Helper.get(db, this.mainIgui.getPartition().getName());
    final dal.BaseApplication[] apps = dalPart.get_all_applications(new String[] {"YarrScanConsole"}, null, null);

    final Map<dal.BaseApplication, String> ctrlCfgMap = new HashMap<dal.BaseApplication, String>();
    final Map<dal.BaseApplication, String> connCfgMap = new HashMap<dal.BaseApplication, String>();
    final Map<dal.BaseApplication, String> scanCfgMap = new HashMap<dal.BaseApplication, String>();

    for(final dal.BaseApplication a : apps) {
      final YarrScanConsole ysc = YarrScanConsole_Helper.cast(a);
      if(ysc != null) {
        ctrlCfgMap.put(a, ysc.get_ctrlCfgPath());
        connCfgMap.put(a, String.join(",", ysc.get_cConfigPaths()));
        scanCfgMap.put(a, ysc.get_scanType());
      }
    }

    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {

        ScanPanel.this.cbModel.removeAllElements();

        for (dal.BaseApplication app : ctrlCfgMap.keySet()) {
          ScanPanel.this.tableModel.addColumn(
            app.UID(),
            new String[] {
              ctrlCfgMap.get(app),
              connCfgMap.get(app),
              scanCfgMap.get(app)
            }
          );

          ScanPanel.this.cbModel.addElement(app.UID());
        }
      }
    });
  }

  // Helper method to send commands to applications
  private void sendUserCommand(final String appName, final String cmdName, final String[] args) {
    IguiLogger.warning("sending command " + cmdName + " to " + appName);
    this.executor.submit(new Runnable() {
      @Override
      public void run() {
        try {
          ScanPanel.this.rcSender.userCommand(appName, cmdName, args);
        }
        catch(daq.rc.RCException ex) {
          ErrorFrame.showError(ScanPanel.PANEL_NAME, "Failed sending command to application", ex);
        }
      }
    });
  }

  // Here define the panel lauyout
  private void initGUI() {

    this.setLayout(new GridLayout(2, 1));

    final JPanel topPanel = new JPanel(new BorderLayout());
    topPanel.setOpaque(true);
    topPanel.setBackground(Color.BLACK);

    final JPanel bottomPanel = new JPanel(new BorderLayout()); // Commands sender (a JComboBox and a set of JButtons)
    bottomPanel.setOpaque(true);
    bottomPanel.setBackground(Color.ORANGE);

    this.add(topPanel);
    this.add(bottomPanel);

    // Top Panel
    {
      final JTable table = new JTable(this.tableModel);

      final JScrollPane sp = new JScrollPane(table);
      topPanel.add(sp, BorderLayout.CENTER);

      this.tableModel.addColumn("Application", new String[] {"Controller Config", "Connectivity Config", "Scan Config"});

      this.tableModel.addTableModelListener(new TableModelListener() {
        @Override
        public void tableChanged(final TableModelEvent e) {
          final int colValue = e.getColumn();
          final int rowValue = e.getFirstRow();

          if((e.getType() == TableModelEvent.UPDATE) && (rowValue != TableModelEvent.HEADER_ROW) && (colValue != TableModelEvent.ALL_COLUMNS))
          {
            ScanPanel.this.reactToTableChanges(colValue, rowValue);
          }

        }
      });
    }

    // Bottom Panel
    {
      final JSplitPane sp = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
      final JLabel label = new JLabel("Select Application", SwingConstants.CENTER);

      final JPanel blp1 = new JPanel(new GridLayout(3, 1));
      blp1.add(label);
      blp1.setBackground(Color.orange);
      blp1.setOpaque(true);

      final JComboBox<String> cb = new JComboBox<>(this.cbModel);
      blp1.add(cb);

      blp1.add(this.b1);
      sp.add(blp1);
      this.b1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          // Get the selected app
          final String appName = ScanPanel.this.cbModel.getSelectedItem().toString();
          ScanPanel.this.sendUserCommand(appName, "RUN_SCAN", new String[] {""});
        }
      });

      bottomPanel.add(sp, BorderLayout.CENTER);
    }
  }
}
